source require_tt.tcl

set cwd [ file normal [ file dir [ info script ] ] ]

tol::include [ file normal [ file join $cwd .. .. "GuiTools.tol" ] ]
tol::include "ExampleMM.tol"

source [ file normal [ file join $cwd .. .. tcl "MenuManager2.tcl" ] ]
source [ file normal [ file join $cwd .. .. tcl "ImageManager.tcl" ] ]

set selection [ list ]
foreach o { ej11 ej12 ej21 ej22 ejemplos1 ejemplos2 } {
  lappend selection [ tol::info address [ list NameBlock $o ] ]
}

