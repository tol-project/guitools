package require Tk

source check_mm_00.tcl
tol::include [ file normal "../test_0003/test.tol" ]

destroy .exampleMM
set menuRoot [ menu .exampleMM -tearoff 0 ]
MenuManager::insertEntriesForSelection $menuRoot $selection
bind . <3> "tk_popup $menuRoot %X %Y"
