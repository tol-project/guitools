#Require GuiTools;

NameBlock MyContextMenuFunctions =
[[
  Real Chart( NameBlock instance, Set extraData )
  {
    ChartGroup( [[instance]], extraData )
  };
  
  Real ChartGroup( Set instances, Set extraData )
  {
    Set all_series = EvalSet(instances, Serie(NameBlock object){
      object::GetData(?)
    });
    Text TclChartSerie(all_series, SetOfSet(
      @TclArgSt("-title", Tcl_Eval("mc \"Serie chart\"")[1])
     ));
    Real 0
  };

  Real writeMsgGroup1( Set instances, Set extraData )
  {
    WriteLn( "writeMsgGroup1 : la seleccion tiene " << Card(instances) << " elementos" );
    Set View( instances, "" );
    0
  };

  Real writeMsgGroup2( Set instances, Set extraData )
  {
    WriteLn( "writeMsgGroup2 : la seleccion tiene " << Card(instances) << " elementos" );
    0
  };

  Real checkOnlyEven( Set instances, Set extraData )
  {
    Real result = Not( Card( instances ) % 2 );
    WriteLn( "result = " << result );
    result
  }
]];

NameBlock Test02 = [[
Class @Ejemplo1
{
  Serie _.data;
  Serie GetData(Real void) {
    _.data
  }
};

Class @Ejemplo2
{
  Serie _.data;
  Serie GetData(Real void) {
    _.data
  }
};

Class @Ejemplos
{
  Test02::@Ejemplo1 _.ej1;
  Test02::@Ejemplo2 _.ej2
}
]];

// Etiqueta a mostrar en el submenu asociado a la seleccion multiple
// de instancia de las clase @Ejemplo1 y @Ejemplo2, solo se crea el
// submenu en el caso de que en la seleccion aparezcan mas de 1 tipo
// de dato con opciones de menu definidas.
Real GuiTools::MenuManager::replaceOptionLabel
( [[ Text name = "Test02::@Ejemplo1", Text label = "Ejemplo1" ]] );

Real GuiTools::MenuManager::replaceOptionLabel
( [[ Text name = "Test02::@Ejemplo2", Text label = "Ejemplo2" ]] );

Real GuiTools::MenuManager::replaceOptionLabel
( [[ Text name = "Test02::@Ejemplos", Text label = "Ejemplos 1 y 2" ]] );

// Chart_Serie es una opcion que se aplica sobre una unica instancia
Real GuiTools::MenuManager::defineMenuCommand
( "Test02::@Ejemplo1",
  [[ Text name = "Chart_Serie",
     Text label = "(INDIV) Grafica",
     Text image = "Serie1",
     Real flagGroup = 0,
     Code CmdInvoke = MyContextMenuFunctions::Chart ]]
);

// @Ejemplo2 comparte la opcion Chart_Serie con @Ejemplo1
Real GuiTools::MenuManager::defineMenuCommand
( "Test02::@Ejemplo2",
  [[ Text name = "Chart_Serie" ]]
);

// SubMenu/ChartGroup_Serie esta habilitada solo si la seleccion tiene
// un numero par de elementos
Real GuiTools::MenuManager::replaceMenuCommand
( "Test02::@Ejemplo1",
  [[ Text name = "SubMenu/ChartGroup_Serie",
     Text label = "(MULTI) Grafica Grupo",
     Text image = "Serie2",
     Real flagGroup = 1,
     Code CmdInvoke = MyContextMenuFunctions::ChartGroup,
     Code CmdCheckState = MyContextMenuFunctions::checkOnlyEven ]]
);

// @Ejemplo2 comparte la opcion SubMenu/ChartGroup_Serie con @Ejemplo1
Real GuiTools::MenuManager::replaceMenuCommand
( "Test02::@Ejemplo2",
  [[ Text name = "SubMenu/ChartGroup_Serie" ]]
);

Real GuiTools::MenuManager::replaceMenuCommand
( "Test02::@Ejemplo1",
  [[ Text name = "WriteMsgGroup1",
     Text label = "(MULTI) Write Msg 1",
     Text image = "checkedBox",
     Real flagGroup = 1,
     Real rank = -2;
     Code CmdInvoke = MyContextMenuFunctions::writeMsgGroup1 ]]
);

Real GuiTools::MenuManager::replaceMenuCommand
( "Test02::@Ejemplo2",
  [[ Text name = "WriteMsgGroup2",
     Text label = "(MULTI) Write Msg 2",
     Text image = "checkedBox",
     Real flagGroup = 1,
     Real rank = -1;
     Code CmdInvoke = MyContextMenuFunctions::writeMsgGroup2 ]]
);

Real GuiTools::MenuManager::replaceMenuCommand
(
 "Test02::@Ejemplos",
 [[
   Text name = "Ejemplos.Ejemplo1",
   Text label = "Opciones de @Ejemplo1",
   Text delegateOn = "_.ej1"
 ]]
);

Real GuiTools::MenuManager::replaceMenuCommand
(
 "Test02::@Ejemplos",
 [[
   Text name = "Ejemplos.Ejemplo2",
   Text label = "Opciones de @Ejemplo2",
   Text delegateOn = "_.ej2"
 ]]
);

Test02::@Ejemplo1 ej11 = [[ Serie _.data = Gaussian(0,1,C) ]];
Test02::@Ejemplo1 ej12 = [[ Serie _.data = Gaussian(0,2,C) ]];
Test02::@Ejemplo1 ej13 = [[ Serie _.data = Gaussian(0,3,C) ]];
Test02::@Ejemplo2 ej21 = [[ Serie _.data = Gaussian(1,1,C) ]];
Test02::@Ejemplo2 ej22 = [[ Serie _.data = Gaussian(1,2,C) ]];

Test02::@Ejemplos ejemplos1 = 
  [[ Test02::@Ejemplo1 _.ej1 = ej11, Test02::@Ejemplo2 _.ej2 = ej21 ]];

Test02::@Ejemplos ejemplos2 = 
  [[ Test02::@Ejemplo1 _.ej1 = ej12, Test02::@Ejemplo2 _.ej2 = ej22 ]];
