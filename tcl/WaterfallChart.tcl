package require Tk
package require snit

if { ![catch {package present rbc} v] } {
  package require rbc
} {
  if {[catch {package require rbc}]} {
    puts "failed to load rbc"
  }  
}

snit::widget WaterfallChart {

  typevariable idInstance 0
  component graph -public graph
  
  delegate method * to hull
  delegate option * to hull

  variable dataSorted [ list ]
  variable graphLabels [ list ]
  #variable lastBucketValue 0

  typemethod NewTop { args } {
    array set opts {
      -title "Waterfall chart"
    }
    array set opts $args
    if { [ llength [ info command ::project::CreateForm ] ] } {
      set mdi [ ::project::CreateForm \
                    -title [ mc "$opts(-title)" ] \
                    -type Waterfall -iniconfig Waterfall ]
      set w [ $mdi getframe ]
    } else {
      set w ".wfall$idInstance"
      toplevel $w
    }
    WaterfallChart $w.wf
    $w.wf SetData $opts(-data) $opts(-barlabels)
    pack $w.wf -fill both -expand yes
    incr idInstance
    return $w.wf
  }


  constructor { args } {
    if { ![catch {package present rbc} v] } {
      install graph using rbc::graph $win.g -plotrelief ridge
    } {
      install graph using rbc::graph $win.g -plotrelief ridge
    }
    $win.g marker create text \
        -text "test" -name "ballon" -anchor "ne" -xoffset 5 -yoffset 5
    $self configurelist $args
    grid $win.g -row 0 -column 0 -sticky "snew"
    grid rowconfigure    $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1
  }

  method SetData { data labels } {
    set options(-data) $data
    set options(-barlabels) $labels
    $self _preprocessData
    $graph axis configure x -command ""
    $self _clearGraph
    $self _drawGraph
    $graph axis configure x -command [ mymethod _getXLabel ]
  }

  method _preprocessData { } {
    set base0 [ lindex $options(-data) 0 ]    
    set nonzeroContrib [ list ]
    set lbl [ llength $options(-barlabels) ]
    set ld [ llength $options(-data) ]
    if { $lbl >= $ld + 1 } {
      set labelBase0 [ lindex $options(-barlabels) 0 ]
      set labelBase1 [ lindex $options(-barlabels) end ]
      set barLabels [ lrange $options(-barlabels) 1 [ expr { $ld - 1 } ] ]
    } elseif { $lbl == $ld } {
      set labelBase0 [ lindex $options(-barlabels) 0 ]
      set labelBase1 "End"
      set barLabels [ lrange $options(-barlabels) 1 end ]
    } else {
      set labelBase0 "Start"
      set labelBase1 "End"
      set barLabels $options(-barlabels)
      for { set i [ expr $ld -1 - $lbl ] } { $i < $ld -1 } { incr i } {
        lappend barLabels "Bucket_$i" 
      }
    }
    foreach c [ lrange $options(-data) 1 end ] l $barLabels {
      if { $c > 0 || $c < 0 } {
        lappend nonzeroContrib [ list $c $l ]
      }
    }
    set dataSorted [ list [ list $base0 $labelBase0 ] ]
    foreach c [ lsort -decreasing -real -index 0 $nonzeroContrib ] {
      lappend dataSorted $c
    }
    lappend dataSorted [ list {} $labelBase1 ]
    puts $dataSorted
  }

  method _clearGraph { } {
    eval $graph marker delete [ $graph marker names "bar_*" ]
    eval $graph marker delete [ $graph marker names "bartxt_*" ]
  }

  method _getXLabel { g index } {
    incr index -1
    return [ lindex [ lindex $dataSorted $index ] 1 ]
  }
  
  method _drawGraph { } {
    set data $dataSorted
    set xmin 0
    set xmax [ expr { [ llength $data ] + 1 } ]
    set ticks [ list ]
    for { set i 1 } { $i <= [ llength $data ] + 1 } { incr i } {
      lappend ticks $i
    }
    $graph axis configure x \
        -min $xmin -max $xmax -subdivisions 1 \
        -majorticks $ticks
    set ymin 0
    set ymax 0
    set dx 1
    set x0 0.5
    set y0 0
    set first 1
    set id 0
    foreach y [ lrange $data 0 end-1 ] {
      set y [ lindex $y 0 ]
      set x1 [ expr { $x0 + $dx } ]
      set y1 [ expr { $y0 + $y } ]
      if { $first } {
        set ymax $y1
        set color "blue"
        set first 0
        set yoffset -10
      } elseif { $y > 0 } {
        set ymax $y1
        set color "green"
        set yoffset -10
      } else {
        set color "red"
        set yoffset 10 
      }
      $graph marker create polygon \
          -coords [ list $x0 $y0 $x0 $y1 $x1 $y1 $x1 $y0 ] \
          -fill $color -name "bar_$id" -linewidth 1 -cap projecting
      $graph marker create text \
          -coords [ list  [ expr { $x0 + 0.5 } ] $y1 ] \
          -anchor center -yoffset $yoffset -background {} -fill {} \
          -text "$y1" -name "txtbar_$id"
      incr id
      foreach { x0 y0 } [ list $x1 $y1 ] { break }
    }
    #set lastBucketValue $y0
    set lastBucket [ lindex $dataSorted end ]
    set dataSorted [ lreplace $dataSorted end end \
                         [ list $y0 [ lindex $lastBucket 1 ] ] ]
    set x1 [ expr { $x0 + $dx } ]
    set y1 0
    $graph marker create polygon \
        -coords [ list $x0 $y0 $x0 $y1 $x1 $y1 $x1 $y0 ] \
        -fill "#d9d900" -name "bar_$id" -linewidth 1 -cap projecting
    $graph marker create text \
        -coords [ list  [ expr { $x0 + 0.5 } ] $y0 ] \
        -anchor center -yoffset -10 -background {} -fill {} \
        -text "$y0" -name "txtbar_$id"
    $graph axis configure y \
        -subdivisions 1 \
        -min 0 -max [ expr { $ymax * 1.1 } ]
    for {} { $id >= 0 } { incr id -1 } { 
      $graph marker bind "bar_$id" \
          <Motion> [ mymethod _onOverBar %x %y $id ]
      $graph marker bind "bar_$id" \
          <Leave> [ mymethod _onLeaveBar %x %y $id ]
    }
    $graph marker before "ballon"
  }

  method _onOverBar { x y barID } {
    #puts "Move $x $y $barID"
    set x [ $graph axis invtransform x $x ]
    set y [ $graph axis invtransform y $y ]
    foreach { v b } [ lindex $dataSorted $barID ] break
    set label "$b=$v"
    $graph marker configure "ballon" -coords [ list $x $y ] -hide no \
      -text $label
  }

  method _onEnterBar { x y barID } {
    puts "Enter $x $y $barID"
  }

  method _onLeaveBar { x y barID } {
    puts "Leave $x $y $barID"
    $graph marker configure "ballon" -hide yes
  }

  # ========================================================================
  # TESTS
  # ========================================================================
  proc Test_00 { } {
    
    set w [ ::WaterfallChart NewTop \
               -data  { 40  5 10  1 0.0 0.5 -15 -7 -1 } \
                -barlabels { B1 I1 I2 I3  I4  I5  D1 D2 D3 B2 } ]
    
    pack $w -fill both -expand yes
    
  }
}
# WaterfallChart::Test_00
